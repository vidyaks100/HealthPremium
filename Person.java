package com.eds.beans;

public class Person {

	private String name;
	private boolean isMale;
	private int age;
	private Health health;
	private Habit habit;
	
	public Person(String name, boolean isMale, int age, 
			Health health, Habit habit) {
		this.name = name;
		this.isMale = isMale;
		this.age = age;
		this.health = health;
		this.habit = habit;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isMale() {
		return isMale;
	}
	public void setMale(boolean isMale) {
		this.isMale = isMale;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Health getHealth() {
		return health;
	}
	public void setHealth(Health health) {
		this.health = health;
	}
	public Habit getHabit() {
		return habit;
	}
	public void setHabit(Habit habit) {
		this.habit = habit;
	}
	
	
}
