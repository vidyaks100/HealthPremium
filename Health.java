package com.eds.beans;

public class Health {

	private boolean hasHyperTension;
	private boolean hasBloodPressure;
	private boolean hasBloodSugar;
	private boolean isOverWeight;
	
	public Health(boolean hasHyperTension, boolean hasBP, boolean hasBloodSugar, boolean isOverWeight){
		this.hasHyperTension = hasHyperTension;
		this.hasBloodPressure = hasBP;
		this.hasBloodSugar = hasBloodSugar;
		this.isOverWeight = isOverWeight;
		
	}
	public boolean isHasHyperTension() {
		return hasHyperTension;
	}
	public void setHasHyperTension(boolean hasHyperTension) {
		this.hasHyperTension = hasHyperTension;
	}
	public boolean isHasBloodPressure() {
		return hasBloodPressure;
	}
	public void setHasBloodPressure(boolean hasBloodPressure) {
		this.hasBloodPressure = hasBloodPressure;
	}
	public boolean isHasBloodSugar() {
		return hasBloodSugar;
	}
	public void setHasBloodSugar(boolean hasBloodSugar) {
		this.hasBloodSugar = hasBloodSugar;
	}
	public boolean isOverWeight() {
		return isOverWeight;
	}
	public void setOverWeight(boolean isOverWeight) {
		this.isOverWeight = isOverWeight;
	}
	
	
}
