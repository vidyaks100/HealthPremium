package com.eds.premium;

import com.eds.beans.Habit;
import com.eds.beans.Health;
import com.eds.beans.Person;

public class HealthPremiumCalculator {

	public static void main(String[] args) {

		Health health = new Health(false, false, false, true);
		Habit habit = new Habit(false, true, true, false);
		Person person = new Person("Norman Gomes", true, 34, health, habit);
		
		PremiumCalcImpl impl = new PremiumCalcImpl();
		double premium = impl.findPremium(person);
		System.out.println("Final Premium: " + premium);
	}
	
}
