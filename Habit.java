package com.eds.beans;

public class Habit {
	
	private boolean doesSmoking;
	private boolean isAlcoholic;
	private boolean doesExcercise;
	private boolean isDrugAddict;
	
	public Habit(boolean doesSmoking,boolean isAlcoholic, boolean doesExcercise, boolean isDrugAddict ) {
		this.doesSmoking = doesSmoking;
		this.isAlcoholic = isAlcoholic;
		this.doesExcercise = doesExcercise;
		this.isDrugAddict = isDrugAddict;
	}
	
	public boolean isDoesSmoking() {
		return doesSmoking;
	}
	public void setDoesSmoking(boolean doesSmoking) {
		this.doesSmoking = doesSmoking;
	}
	public boolean isAlcoholic() {
		return isAlcoholic;
	}
	public void setAlcoholic(boolean isAlcoholic) {
		this.isAlcoholic = isAlcoholic;
	}
	public boolean isDoesExcercise() {
		return doesExcercise;
	}
	public void setDoesExcercise(boolean doesExcercise) {
		this.doesExcercise = doesExcercise;
	}
	public boolean isDrugAddict() {
		return isDrugAddict;
	}
	public void setDrugAddict(boolean isDrugAddict) {
		this.isDrugAddict = isDrugAddict;
	}
	
	
}
