package com.eds.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.eds.beans.Habit;
import com.eds.beans.Health;
import com.eds.beans.Person;
import com.eds.premium.PremiumCalcImpl;

public class PremiumCalTest {

	@Test
	public void getPremium(){
		
		Health health = new Health(false, false, false, true);
		Habit habit = new Habit(false, true, true, false);
		Person person = new Person("Norman Gomes", true, 34, health, habit);
		
		PremiumCalcImpl impl = new PremiumCalcImpl();
		double premium = impl.findPremium(person);
		System.out.println(premium);
		assertNotNull(premium);
	}
}
