package com.eds.premium;

import com.eds.beans.Habit;
import com.eds.beans.Health;
import com.eds.beans.Person;

public class PremiumCalcImpl {

	public double findPremium(Person person){
		double premium = 0;
		
		int percent = getPercentOnAge(person.getAge());
		if(person.isMale())
			percent+=2;
		int percentOnHealth = getPercentOnHealth(person.getHealth());
		percent+=percentOnHealth;
		int percentOnHabit = getPercentOnHabit(person.getHabit());
		percent += percentOnHabit;
		System.out.println("total percent:"+ percent);
		premium = ((percent*5000)/100) + 5000;
		return premium;
	}
	
	int getPercentOnAge(int age) {
		int percent = 0;
		if(age >=  18) {
			if(age < 25)
				percent+=10;
			else {
				int tempAge = 25;
				while(age >= tempAge) {
					if(tempAge < 40) {
						percent+=10;
					}else {
						percent+=20;
					}
					tempAge+=5;
				}
				
				if(age < tempAge && age > tempAge-5) {
					if(tempAge < 40) {
						percent+=10;
					}else {
						percent+=20;
					}
				}
			}	
		}
		System.out.println("percent on age:"+ percent);
		return percent;
	}	
	
	int getPercentOnHealth(Health health) {
		int percent = 0;
		if(health.isHasBloodPressure())percent++;
		if(health.isHasBloodSugar())percent++;
		if(health.isHasHyperTension())percent++;
		if(health.isOverWeight())percent++;
		
		System.out.println("percent based on health: "+ percent);
		return percent;
	}
	
	int getPercentOnHabit(Habit habit) {
		int percent = 0;
		
		if(habit.isDoesSmoking())percent+=3;
		if(habit.isAlcoholic())percent+=3;
		if(habit.isDoesExcercise())percent-=3;//Reducing the percent for good habit
		if(habit.isDrugAddict())percent+=3;
		System.out.println("percent based on habit after decrementing for good habits "+ percent);
		return percent;
	}

}
